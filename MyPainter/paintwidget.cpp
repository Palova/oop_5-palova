#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::SaltandPepper()
{

	double salt = 0.05, pepper = 0.05;
	for (int i = 0; i < image.size().width(); i++)
	{
		for (int j = 0; j < image.size().height(); j++)
		{
			double val = (double)rand() / RAND_MAX;
			if (val < salt)	image.setPixelColor(i, j, qRgb(0,0,0));
			else if (val > 1 - pepper) image.setPixelColor(i, j, qRgb(255,255,255));
			
		}
	}
	update();
}

void PaintWidget::Sepia()
{
	for (int i = 0; i < image.size().width(); i++)
	{
		for (int j = 0; j < image.size().height(); j++)
		{
			QColor color = image.pixelColor(i, j);

			int r = color.red();
			int g = color.green();
			int b = color.blue();
			
			int tr = (int)(0.393*r + 0.769*g + 0.189*b);
			int tg = (int)(0.349*r + 0.686*g + 0.168*b);
			int tb = (int)(0.272*r + 0.534*g + 0.131*b);

			if (tr > 255) 
			{
				r = 255;
			}
			else
			{
				r = tr;
			}

			if (tg > 255)
			{
				g = 255;
			}
			else
			{
				g = tg;
			}

			if (tb > 255)
			{
				b = 255;
			}
			else
			{
				b = tb;
			}

			image.setPixelColor(i, j, qRgb(r, g, b));										
		}
	}
	update();
}

void PaintWidget::Medianovy_filter()
{
	int red, green, blue;
	QList<int> Lr, Lg, Lb;

	for (int i = 0; i < image.size().width()-1; i++)
	{
		for (int j = 0; j < image.size().height()-1; j++)
		{
			for (int a = i - 1; a <= i + 1; a++)
			{
				for (int b = j - 1; b <= j + 1; b++)
				{
					QColor farba=image.pixelColor(a,b);
					red = farba.red();
					green = farba.green();
					blue = farba.blue();

					Lr.append(red); Lg.append(green); Lb.append(blue);
				}
			}
					qSort(Lr); qSort(Lg); qSort(Lb);
					image.setPixelColor(i, j, qRgb(Lr[4], Lg[4], Lb[4])); //9 cisel pre kazdu farbu(3x3) median
																		//je 5 prvok(QList index od 0)	

					Lr.clear();	Lg.clear(); Lb.clear();
		}		
	}
	update();
}