#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void ActionOpen();
	void ActionSave();
	void EffectClear();
	void ActionNew();
	void saltandpepper();
	void sepia();
	void median();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
